import { Question } from './question'

export class Category {
    title:string;
    questions:Question[] = [];

    constructor(title:string, questions:Question[] = null){
        if(questions){
            this.questions = questions;
        }

        this.title = title;
    }

    getQuestions() {
        return this.questions;
    }

    getQuestion(index:number){
        return this.questions[index];
    }

    addQuestion(q:Question) {
        this.questions.push(q);
    }

    toString(){
        return this.title+" - "+this.questions;
    }

}