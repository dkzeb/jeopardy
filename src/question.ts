export class Question {
    question: string;
    answer: string;
    value: number;
    answered: boolean;

    constructor(question:string, answer:string, value:number){
        this.question = question;
        this.answer = answer;
        this.value = value;
        this.answered = false;
    }

    getQuestion() {
        return this.question;
    }

    getAnswer() {
        return this.answer;
    }

    getValue() {
        return this.value;
    }    

    setAnswered(answered:boolean){
        this.answered = this.answered;
    }
}