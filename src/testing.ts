import { Category } from './category'
import { Question } from './question'

let cat1 = new Category("Category 1")
let cat2 = new Category("Category 2")

cat1.addQuestion(new Question("1200", "600+600", 500))
cat1.addQuestion(new Question("Dansk Hovedstad", "København", 1000))
cat2.addQuestion(new Question("Jens Hansen havde en", "Bondegård", 500))

console.log(cat1);
console.log(cat2);