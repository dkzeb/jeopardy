const url = require('url');
let clients = {};
let dash;
let game;

module.exports = ((app, ews) => {

    ews.getWss().on('connection', (ws) => {
        const urlObj = url.parse(ws.upgradeReq.url, true);        
        const id = urlObj.query.id;                

        if(id.indexOf('btn') > 0){
            clients[id] = ws;
            console.log("Client", urlObj.query.id, "connected");
            console.log("# of clients", Object.keys(clients).length);
            setTimeout(() => {
                ws.send("READY");
            }, 1000);
        }        

        if(id == 'dash'){
            console.log("Dash connected");
            dash = ws;
        }

        if(id == 'game'){
            console.log("Game connected");
            game = ws;
        }
    });
    app.ws('/ws/buttons', wsHandler);    
    app.ws('/ws/dash', dashHandler);
    app.ws('/ws/game', gameHandler);

})

export const getClients = () => {
    return clients;
}

const wsHandler = (ws, req) => {

    // message
    ws.on('message', function(msg) {                
        const urlObj = url.parse(ws.upgradeReq.url, true);       
        const btnID = urlObj.query.id;
        console.log(btnID,"clicked to answer");
        Object.keys(clients).forEach(k => {
            if(k != btnID){
                console.log("SENDING OFF TO", k);
                let client = clients[k];            
                client.send("OFF");
            } else {
                console.log("SENDING ON TO", k);
                clients[k].send("ON");
            }                
        });
    });

}

const gameHandler = (ws, req) => {
    ws.on('message', (msg) => {
        console.log("Game sent a msg", msg);
    })
}

const dashHandler = (ws, req) => {
    ws.on('message', (msg) => {
        if(msg.indexOf('showq') >= 0 || msg.indexOf('hideq') >= 0){
            if(game)
                game.send(msg);
        }

        if(msg.indexOf('resetbtns') >= 0){
            Object.keys(clients).forEach(k => {
                let c = clients[k];
                c.send("READY");
            })
        }
    })
}