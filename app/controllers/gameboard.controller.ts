import { Router, Request, Response } from 'express';
import { readFileSync } from 'fs';

const router : Router = Router();

router.get('/', (req:Request, res: Response) => {
    let q = readFileSync('config/questions.json', 'utf8');
    let data = JSON.parse(q);

    res.render('gameboard', {
         // hent questions.json filen            
        questions: data,
        questionsJSON: JSON.stringify(data)        
    })
})

export const GameboardController: Router = router;