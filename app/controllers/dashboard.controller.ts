import { Router, Request, Response } from 'express';
import { ResetButtons } from '../server';

import { readFileSync } from 'fs';

const router : Router = Router();

router.get('/', (req:Request, res: Response) => {
    
    // hent questions.json filen
    let q = readFileSync('config/questions.json', 'utf8');
    let data = JSON.parse(q);
    res.render('dashboard', {                
        questions: data
    })
})

router.get('/resetbuttons', (req:Request, res:Response) => {
    ResetButtons();
    res.sendStatus(200);
});

export const DashboardController: Router = router;