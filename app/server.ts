import * as express from 'express'
import * as ebs from 'express-handlebars';
import * as ws from 'express-ws';
import url = require('url');

import { WelcomeController, DashboardController, GameboardController } from './controllers'

const app: express.Application = express()
const port: number = process.env.PORT || 3000
export const expressWS = ws(app);

// though im not happy about it - we do websockets here for now
const wss = require('./controllers/sockets.controller')(app, expressWS);

export const ResetButtons = () => {
    expressWS.getWss().clients.forEach(ws => {
        /*const urlObj = url.parse(ws.upgradeReq.url);
        console.log(urlObj);*/
        ws.send("READY");
    });
}

app.engine('handlebars', ebs({defaultLayout: 'main'}))
app.set('view engine', 'handlebars')

app.use('/welcome', WelcomeController)
app.use('/dashboard', DashboardController)
app.use('/game', GameboardController)

app.listen(port, () => {
    console.log(`server is listening on ${port}`)
})